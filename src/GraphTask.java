import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      Vertex v4 = g.createVertex("v4");
      Vertex v3 = g.createVertex("v3");
      Vertex v2 = g.createVertex("v2");
      Vertex v1 = g.createVertex("v1");
      Vertex v0 = g.createVertex("v0");
      g.createArc("av0_v1",v0, v1);
      g.createArc("av1_v2",v1, v2);
      g.createArc("av1_v3",v1, v3);
      g.createArc("av2_v0",v2, v0);
      g.createArc("av3_v4",v3, v4);
      g.createArc("av4_v1",v4, v1);
      List<Arc> tour = g.findEulerTour();
      System.out.println(tour);
   }

   /** Find Euler tour of a directed graph. */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int inDegree;
      private int outDegree;
      private int edgeCount;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }

   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      private boolean removed;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   } 

   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }


      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         from.outDegree++;
         to.inDegree++;
         from.edgeCount++;
         return res;
      }

      /**
       * Finds the correct Arc to be removed and marks the Arc
       * in the graph as visited.
       * @param from Vertex where the graph is coming out from
       * @param to Vertex where the graph is coming in to
       */
      public void removeArc (Vertex from, Vertex to) {
         Arc a = from.first;

         while (!(a.target.id.equals(to.id))) {
            a = a.next;
         }
         a.removed = true;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Check if the given graph contains an Euler tour.
       * An Euler tour always exists if the in-degree equals
       * the out-degree of every vertex and if the graph is
       * connected.
       * @param g The graph that is being checked
       */

      public boolean checkIfEulerian(Graph g) {
         if(g.first != null){
            Vertex f = g.first;

            while (f != null) {
               if (f.inDegree != f.outDegree || f.inDegree == 0){
                  return false;
               }
               f = f.next;
            }
            return true;
         }
         return false;
      }

      /**
       * Find and print the Euler Tour of a graph if it has one.
       * Euler tour is a cycle that traverses each edge of a graph
       * exactly once according to its direction.
       * Uses Hierholzer's algorithm.
       * @link https://www.geeksforgeeks.org/hierholzers-algorithm
       * -directed-graph/
       */
      public List<Arc>findEulerTour() {
         StringBuffer s = new StringBuffer();
         List<Arc> EulerTour = new LinkedList<>();

         if (checkIfEulerian(this)){
            System.out.println("Euler Tour found!");
            System.out.println("Euler Tour of " + this.id + " is:" );

            Stack<Vertex> curr_path = new Stack<>(); // Stores vertices
            Stack<Vertex> circuit = new Stack<>(); // Stores the final tour

            Vertex curr_v = this.first;
            curr_path.push(curr_v); // Current vertex

            while (!curr_path.empty()) {

               if (curr_v.edgeCount > 0) {   //If there's remaining edge(s)

                  curr_path.push(curr_v);  // Push the vertex
                  Arc a = curr_v.first;

                  while (a.removed){   // Find an edge
                     a = a.next;
                  }

                  Vertex next = a.target;  // Find the next vertex
                  curr_v.edgeCount--;  //Decrement edge count
                  removeArc(curr_v, next); // Remove the edge
                  curr_v = next; //Move to next vertex

               } else {
                  circuit.add(curr_v);
                  curr_v = curr_path.pop();  // Back-tracking
               }
            }
            while (circuit.size() > 0) {

               if (circuit.size() != 1) {
                  s.append(circuit.peek()).append(" -> ");

                  Arc current = circuit.pop().first;
                  while (EulerTour.contains(current)){
                     current = current.next;
                  }
                  EulerTour.add(current);

               } else {
                  s.append(circuit.pop());
               }
            }
            System.out.println(s);

         } else {
            System.out.println("Euler Tour not found!");
         }
         return EulerTour;
      }
   }
} 

